import java.security.*;

public class KeysGenerator {

    private static final String ALG = "RSA";
    private static final int KEY_LENGTH = 1024;

    private KeyPair pair;
    private KeyPairGenerator keyGen;

    public KeysGenerator() throws NoSuchAlgorithmException, NoSuchProviderException {
        keyGen = KeyPairGenerator.getInstance(ALG);
        keyGen.initialize(KEY_LENGTH);
    }

    public KeysGenerator generate() {
        pair = keyGen.generateKeyPair();
        return this;
    }

    public PrivateKey getPrivateKey() {
        return pair.getPrivate();
    }

    public PublicKey getPublicKey() {
        return pair.getPublic();
    }

    public KeyPair getKeyPair() {
        return pair;
    }
}